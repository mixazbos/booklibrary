﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OTUS_DZ9.Interfaces
{
    public interface IMyCloneable<T>
    {
        public T Copy();
    }
}
