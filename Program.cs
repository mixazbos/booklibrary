﻿using OTUS_DZ9.Classes;
using System;


namespace OTUS_DZ_9
{
    public class Program
    {
        static void Main(string[] agrs)
        {
            Console.WriteLine("Список книг:");
            Console.WriteLine();

            var film = new Book("Книга ", 1812, " Автор - 1 ");
            var newFilm = film.Copy();
            newFilm.Name = "Книга";
            Console.WriteLine(film.ToString());
            Console.WriteLine(newFilm.ToString());
            Console.WriteLine();

            var scienceBook = new ScienceBook("Научная книга ", 2001, " Основы механики ", true);
            var newScienceBook = scienceBook.Copy();
            newScienceBook.Author = " Автор не известен ";
            Console.WriteLine(scienceBook.ToString());
            Console.WriteLine(newScienceBook.ToString());
            Console.WriteLine();

            var detectiveBook = new DetectiveBook("Приключения Холмса ", 1900, " Игорь Масленников ", true, true);
            var newDetectiveBook = detectiveBook.Copy();
            newDetectiveBook.ReleaseYear = 2015;
            Console.WriteLine(detectiveBook.ToString());
            Console.WriteLine(newDetectiveBook.ToString());
            Console.WriteLine();

            var horrorBook = new HorrorBook("Кошмар на улице Вязов ", 1970, " Американец ", true, true);
            var newHorrorBook = horrorBook.Copy();
            newHorrorBook.Author = " Роберт Ш ";
            Console.WriteLine(horrorBook.ToString());
            Console.WriteLine(newHorrorBook.ToString());
            Console.WriteLine();

            Console.ReadLine();
        }
    }
}