﻿using OTUS_DZ9.Interfaces;
using System;

namespace OTUS_DZ9.Classes
{
    // детективная книга
    public class DetectiveBook : ScienceBook, IMyCloneable<DetectiveBook>, ICloneable
    {
        public bool IsDetective { get; set; }

        public DetectiveBook(string name, int releaseYear, string author, bool isFiction, bool isDocumental) : base(name, releaseYear, author, isFiction)
        {
            IsDetective = isDocumental;
        }

        public override DetectiveBook Copy()
        {
            return new(Name, ReleaseYear, Author, IsScience, IsDetective);
        }

        public object Clone()
        {
            return Copy();
        }

        public new string ToString()
        {
            return String.Concat(new string[] { Name, ReleaseYear.ToString(), Author, IsScience.ToString(), IsDetective.ToString() });
        }
    }
}
