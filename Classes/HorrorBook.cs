﻿using OTUS_DZ9.Interfaces;
using System;

namespace OTUS_DZ9.Classes
{
    // Книга ужасов
    public class HorrorBook : ScienceBook, IMyCloneable<HorrorBook>, ICloneable
    {
        public bool IsHorror { get; set; }

        public HorrorBook(string name, int releaseYear, string author, bool isFiction, bool isFatnasy) : base(name, releaseYear, author, isFiction)
        {
            IsHorror = isFatnasy;
        }

        public override HorrorBook Copy()
        {
            return new(Name, ReleaseYear, Author, IsScience, IsHorror);
        }

        public object Clone()
        {
            return Copy();
        }

        public new string ToString()
        {
            return String.Concat(new string[] { Name, ReleaseYear.ToString(), Author, IsScience.ToString(), IsHorror.ToString() });
        }
    }
}
