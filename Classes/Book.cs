﻿using OTUS_DZ9.Interfaces;
using System;

namespace OTUS_DZ9.Classes
{

    // Главный класс книги 
    public class Book : IMyCloneable<Book>, ICloneable
    {
        public string Name { get; set; }
        public int ReleaseYear { get; set; }
        public string Author { get; set; }

        public Book(string name, int releaseYear, string author)
        {
            Name = name;
            ReleaseYear = releaseYear;
            Author = author;
        }

        public virtual Book Copy()
        {
            return new(Name, ReleaseYear, Author);
        }

        public object Clone()
        {
            return Copy();
        }

        public new string ToString()
        {
            return String.Concat(new string[] { Name, ReleaseYear.ToString(), Author });
        }
    }
}
