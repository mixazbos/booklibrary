﻿using System;
using OTUS_DZ9.Interfaces;

namespace OTUS_DZ9.Classes
{
    // научная книга
    public class ScienceBook : Book, IMyCloneable<ScienceBook>, ICloneable
    {
        public bool IsScience { get; set; }

        public ScienceBook(string name, int releaseYear, string author, bool isFiction) : base(name, releaseYear, author)
        {
            IsScience = isFiction;
        }

        public override ScienceBook Copy()
        {
            return new(Name, ReleaseYear, Author, IsScience);
        }

        public object Clone()
        {
            return Copy();
        }

        public new string ToString()
        {
            return String.Concat(new string[] { Name, ReleaseYear.ToString(), Author, IsScience.ToString() });
        }
    }
}
